﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace b2blifeforcebd.DAL_lib
{
    public class bblifeforcebd
    {

        public class c_accepted_delivery_terms
        {
            public virtual Int64 accepted_delivery_terms_id { get; set; }
            public virtual bool FOB { get; set; }
            public virtual bool EXW { get; set; }
            public virtual bool FCA { get; set; }
            public virtual bool DDP { get; set; }
            public virtual bool DAF { get; set; }
            public virtual bool CFR { get; set; }
            public virtual bool FAS { get; set; }
            public virtual bool CPT { get; set; }
            public virtual bool DDU { get; set; }
            public virtual bool DES { get; set; }
            public virtual bool CIF { get; set; }
            public virtual bool CIP { get; set; }
            public virtual bool DEQ { get; set; }
            public virtual bool Express_Delivery { get; set; }

        }
        public class c_accepted_payment_currency
        {
            public virtual Int64 accepted_payment_currency_id { get; set; }
            public virtual bool USD { get; set; }
            public virtual bool CAD { get; set; }
            public virtual bool GBP { get; set; }
            public virtual bool EUR { get; set; }
            public virtual bool AUD { get; set; }
            public virtual bool CNY { get; set; }
            public virtual bool JPY { get; set; }
            public virtual bool HKD { get; set; }
            public virtual bool CHF { get; set; }
        }

        public class c_accepted_payment_type
        {
            public virtual Int64 accepted_payment_type_id { get; set; }
            public virtual bool T_T { get; set;}
            public virtual bool moneyGram { get; set;}
            public virtual bool western_Union { get; set;}
            public virtual bool L_C { get; set;}
            public virtual bool credit_card { get; set;}
            public virtual bool cash { get; set;}
            public virtual bool DPDA { get; set;}
            public virtual bool paypal{ get; set;}
            public virtual bool escrow { get; set;}
        }



        public class c_partner_factory
        {
            public virtual Int64 partner_factory_id { get; set; }
            public virtual string factory_name { get; set; }
            public virtual string cooperation_contract { get; set; }
            public virtual string years_of_cooperation { get; set; }
            public virtual string transaction_amnt_prev_year { get; set; }
            public virtual string product1 { get; set; }
            public virtual string product2 { get; set; }
            public virtual string product3 { get; set; }
            public virtual string annual_production_volume1 { get; set; }
            public virtual string annual_production_volume2 { get; set; }
            public virtual string annual_production_volume3 { get; set; }

        }
        public class c_brand
        {
            public virtual Int64 brand_id { get; set; }
            public virtual string brand_name { get; set; }
            public virtual Int64 reg_certification_photo { get; set; }
            public virtual Int64 c_intro_photo_id { get; set; }

        }
        public class c_company_details
        {
            public virtual Int64 company_id { get; set; }
            public virtual Int64 user_id { get; set; }
            public virtual string company_name { get; set; }
            public virtual string subdomain_name { get; set; }
            public virtual Int64 address_id { get; set; }
            public virtual string main_products { get; set; }
            public virtual string other_products { get; set; }
            public virtual Int64 company_reg_year { get; set; }
            public virtual string company_website { get; set; }
            public virtual string legal_owner { get; set; }
            public virtual string office_size { get; set; }
            public virtual string company_certification { get; set; }
            public virtual Int64 factory_info_id { get; set; }
            public virtual Int64 business_type_id { get; set; }
            public virtual string total_no_of_employees { get; set; }
        }
        public class c_introduction_and_images
        {
            public virtual Int64 c_intro_image_id { get; set; }
            public virtual Int64 company_logo { get; set; }
            public virtual string detailed_company_intro { get; set; }
            public virtual Int64 company_brand_id { get; set; }
            public virtual Int64 trade_show_attended_id { get; set; }

        }
        public class c_language_spoken
        {
            public virtual Int64 language_id { get; set; }
            public virtual bool English { get; set; }
            public virtual bool Japanese { get; set; }
            public virtual bool Arabic { get; set; }
            public virtual bool Korean { get; set; }
            public virtual bool Chinese { get; set; }
            public virtual bool Portuguse { get; set; }
            public virtual bool French { get; set; }
            public virtual bool Hindi { get; set; }
            public virtual bool Spanish { get; set; }
            public virtual bool German { get; set; }
            public virtual bool Russian { get; set; }
            public virtual bool Italian { get; set; }
        }
        public class c_trade_information
        {
            public virtual Int64 trade_info_id { get; set; }
            public virtual string total_annual_sales_volume { get; set; }
            public virtual string export_percentage { get; set; }
            public virtual string main_markets { get; set; }
            public virtual string main_customers { get; set; }
            public virtual string no_of_employees_in_trade_dept { get; set; }
            public virtual string nearest_port { get; set; }
            public virtual Int64 avg_lead_time { get; set; }
            public virtual string province { get; set; }
            public virtual Int64 country_id { get; set; }
            public virtual string minimum_order_value { get; set; }
            public virtual Int64 accepted_delivery_term_id { get; set; }
            public virtual Int64 accepted_payment_currency_id { get; set; }
            public virtual Int64 accepted_payment_type_id { get; set; }
            public virtual Int64 language_spoken_id { get; set; }
        }
        public class c_trade_show
        {
            public virtual Int64 trade_show_id { get; set; }
            public virtual string trade_show_name { get; set; }
            public virtual DateTime date_attended { get; set; }
            public virtual Int64 host_country { get; set; }
        }
        public class p_product
        {
            public virtual Int64 product_id { get; set; }
            public virtual string product_title { get; set; }
            public virtual Int64 cat_id { get; set; }
            public virtual Int64 country_id { get; set; }
            public virtual Int64 min_quantity { get; set; }
            public virtual Int64 min_price { get; set; }
            public virtual Int64 max_price { get; set; }
            public virtual DateTime created_date { get; set; }
            public virtual Int64 availability { get; set; }
            public virtual Int64 specification_id { get; set; }
            public virtual Int64 user_id { get; set; }
            public virtual Int64 photo_id { get; set; }
        }
        public class p_product_specification
        {
            public virtual Int64 spec_id { get; set; }
            public virtual string spec_name { get; set; }
            public virtual Int64 user_id { get; set; }
            public virtual string rich_description { get; set; }
            public virtual string meta_keyword { get; set; }
            public virtual string meta_description { get; set; }
        }

        public class tb_business_type
        {
            public virtual Int64 business_type_id { get; set; }
            public virtual bool manufacturer { get; set; }
            public virtual bool trading_company { get; set; }
            public virtual bool buying_office { get; set; }
            public virtual bool agent { get; set; }
            public virtual bool govt_ministry_bureau_commission { get; set; }
            public virtual bool distributor_wholesaler { get; set; }
            public virtual bool association { get; set; }
            public virtual bool business_service { get; set; }
            public virtual bool other { get; set; }
        }
        public class tb_contact
        {
            public virtual Int64 contact_id { get; set; }
            public virtual string email { get; set; }
            public virtual string street { get; set; }
            public virtual string city { get; set; }
            public virtual string state { get; set; }
            public virtual string zipcode { get; set; }
            public virtual Int64 country_id { get; set; }
            public virtual string telephone { get; set; }
            public virtual string Fax { get; set; }
        }
        public class tb_country
        {
            public virtual Int64 country_id { get; set; }
            public virtual string country_name { get; set; }
            public virtual string country_code { get; set; }
            public virtual string ISDcode { get; set; }
        }
        public class tb_photo_bank
        {
            public virtual Int64 photo_id { get; set; }
            public virtual string up_name { get; set; }
            public virtual string server_name { get; set; }
            public virtual Int64 group_id { get; set; }
            public virtual string extension { get; set; }
            public virtual Int16 height { get; set; }
            public virtual Int16 width { get; set; }
        }
        public class u_more_information
        {
            public virtual Int64 more_info_id { get; set; }
            public virtual string industry_we_are_in { get; set; }
            public virtual string pname1 { get; set; }
            public virtual string pname2 { get; set; }
            public virtual string pname3 { get; set; }
            public virtual Int16 cat_id1 { get; set; }
            public virtual Int16 cat_id2 { get; set; }
            public virtual Int16 cat_id3 { get; set; }
            public virtual string purchasing_frequency { get; set; }
            public virtual string annual_purchase_volume { get; set; }
            public virtual Int16 country_id1 { get; set; }
            public virtual Int16 country_id2 { get; set; }
            public virtual Int16 country_id3 { get; set; }
            public virtual Int16 business_type_id { get; set; }
            public virtual Int32 company_address { get; set; }
            public virtual string company_introduction { get; set; }
            public virtual Int32 company_logo { get; set; }
            public virtual Int32 company_license1 { get; set; }
            public virtual Int32 company_license2 { get; set; }
            public virtual Int32 company_license3 { get; set; }
        }
        public class u_user_profile
        {
            public virtual Int64 user_id { get; set; }
            public virtual string first_name { get; set; }
            public virtual string last_name { get; set; }
            public virtual char gender { get; set; }
            public virtual string job_title { get; set; }
            public virtual string email { get; set; }
            public virtual Int32 address { get; set; }
            public virtual string company_name { get; set; }
            public virtual Int16 preferred_business_type { get; set; }
            public virtual string website { get; set; }
            public virtual string password { get; set; }
            
        }
        public class  p_category
        {
            public virtual Int64 cat_id { get; set; }
            public virtual string cat_name { get; set; }
            public virtual Int64 parent_cat_id { get; set; }
            public virtual string category_chain { get; set; }
        }
        public class p_required_field 
        {
            public virtual Int64 required_field_id { get; set; }
            public virtual string required_field_name { get; set; }
            public virtual string field_type { get; set; }
            public virtual bool is_required { get; set; }
        }
        public class p_child_field
        {
            public virtual Int64 child_field_id { get; set; }
            public virtual string child_field_name { get; set; }
            public virtual Int64 nested_field_id { get; set; }
            public virtual Int64 required_field_id { get; set; }
        }
        public class p_fields_under_cat
        {
            public virtual Int64 fields_under_cat_id { get; set; }
            public virtual Int64 cat_id { get; set; }
            public virtual string field_name { get; set; }
            public virtual Int64 required_field_id { get; set; }
        }
        
        


    }
}
