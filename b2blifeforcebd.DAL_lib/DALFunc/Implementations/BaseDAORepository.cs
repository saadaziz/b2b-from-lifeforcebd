﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using b2blifeforcebd.DAL_lib;
using Castle.Facilities.NHibernateIntegration;
using b2blifeforcebd.DAL_lib.DALFunc.Interfaces;

namespace b2blifeforcebd.DAL_lib.DALFunc.Implementations
{
    abstract public class BaseDAORepository<GenericEntityType> : IRepo<GenericEntityType>, IDisposable
       where GenericEntityType : class
    {

        private Connection nb = null;
        protected static log4net.ILog _Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Constructor
        public BaseDAORepository()
        {
            nb = new Connection();
        }
        #endregion
        #region implementations
        public IList<GenericEntityType> All()
        {
            try
            {
                IList<GenericEntityType> res = new List<GenericEntityType>();
                using (ISession session = nb.OpenSession())
                {
                    res = session
                        .CreateCriteria(typeof(GenericEntityType))
                        .List<GenericEntityType>();
                }
                return res;
            }
            catch (ArgumentNullException e)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " object list not PICKED -- " + e);
                return null;
            }
        }
        /// <summary>
        /// Add the entity to DB
        /// </summary>
        /// <param name="entity">DB entity</param>
        /// <returns>bool</returns>
        public bool Add(GenericEntityType entity)
        {
            if (entity == null)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " is null to ADD!");
                return false;
            }
            try
            {
                using (ISession session = nb.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(entity);

                        transaction.Commit();

                    }
                }
                return true;
            }
            catch (Exception e)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " could not be ADDED -- " + e);
                return false;
            }
        }

        public IList<GenericEntityType> FindAll(string idPropertyName, string pvalue)
        {

            if (idPropertyName == null || pvalue == null)
            {
                _Logger.Error(idPropertyName + " is null to select!");
                return null;
            }
            IList<GenericEntityType> list = new List<GenericEntityType>();
            // implimentation here 
            using (ISession session = nb.OpenSession())
            {
                session.BeginTransaction();
                list = session.CreateCriteria(typeof(GenericEntityType))
                                      .Add(Restrictions.Eq(idPropertyName, pvalue))
                                      .List<GenericEntityType>();

                session.Transaction.Commit();
            }
            return list;
        }
        public IList<GenericEntityType> FindAll(int firstResult, int numberOfResults)
        {
            if (firstResult == 0 || numberOfResults == 0)
            {
                _Logger.Error(firstResult + " is 0 to select!");
                return null;
            }

            return nb.OpenSession().CreateCriteria(typeof(GenericEntityType))

                .SetFirstResult(firstResult)

                .SetMaxResults(numberOfResults)

                .List<GenericEntityType>();

        }
        public IList<GenericEntityType> FindAll(int firstResult, int numberOfResults, params Order[] orders)
        {

            return AddOrders(nb.OpenSession().CreateCriteria(typeof(GenericEntityType)))

                .SetMaxResults(numberOfResults)

                .SetFirstResult(firstResult)

                .List<GenericEntityType>();

        }
        public long Count(DetachedCriteria criteria)
        {
            return Convert.ToInt64(criteria.GetExecutableCriteria(nb.OpenSession())

                            .SetProjection(Projections.RowCountInt64()).UniqueResult());

        }
        public bool Add(IList<GenericEntityType> entityList)
        {
            if (entityList == null)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " list is null to ADD!");
                return false;
            }

            try
            {

                using (ISession session = nb.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Save(entityList);

                        transaction.Commit();

                    }
                }
                return true;
            }
            catch (Exception e)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " list could not be ADDED -- " + e);
                return false;
            }
        }

        public GenericEntityType pickbyID(Int64 id)
        {
            using (var session = nb.OpenSession())
            {
                GenericEntityType returnVal = session.Get<GenericEntityType>(id);

                return returnVal;
            }
            return null;

        }
        public IList<GenericEntityType> FindByQuery(string queryString)
        {

            return nb.OpenSession().CreateQuery(queryString).List<GenericEntityType>();

        }
        public bool Delete(GenericEntityType value)
        {
            if (value == null)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " is null to Delete!");
                return false;
            }
            try
            {
                using (var session = nb.OpenSession())
                using (var transaction = session.BeginTransaction())
                {
                    session.Delete(value);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " could not be DeleteD -- " + e);
                return false;
            }
        }
        public bool update(GenericEntityType value)
        {
            if (value == null)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " is null to Update!");
                return false;
            }
            try
            {
                using (var session = nb.OpenSession())
                using (var transaction = session.BeginTransaction())
                {
                    session.Update(value);
                    transaction.Commit();
                }

                return true;
            }
            catch (Exception e)
            {
                _Logger.Error(typeof(GenericEntityType).Name + " could not be UPDATED -- " + e);
                return false;
            }
        }

        #endregion
        #region Public Methods
        public void Dispose()
        {
            nb.CloseSession();
        }
        #endregion
        #region AddOrders
        private static ICriteria AddOrders(ICriteria dc, params Order[] orders)
        {
            if (orders != null)

                foreach (var order in orders)

                    dc.AddOrder(order);



            return dc;

        }
        private static DetachedCriteria AddOrders(DetachedCriteria dc, params Order[] orders)
        {
            if (orders != null)

                foreach (var order in orders)
                    dc.AddOrder(order);
            return dc;

        }
        #endregion
    }
}

