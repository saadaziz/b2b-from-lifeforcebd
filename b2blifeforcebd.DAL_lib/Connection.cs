﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace b2blifeforcebd.DAL_lib
{
    public class Connection
    {
        private ISessionFactory _sessionFactory;
        private ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                    InitializeSessionFactory();

                return _sessionFactory;
            }
        }
        public void CloseSession()
        {
            if (_sessionFactory != null)
            {
                if (!_sessionFactory.IsClosed)
                {
                    _sessionFactory.Close();
                    _sessionFactory.Dispose();
                }
            }
        }
        public ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
        //private static AutoPersistenceModel CreateMappings()
        //{
        //    return AutoMap
        //        .Assembly(System.Reflection.Assembly.GetCallingAssembly())
        //        .Where(t => t.Namespace == "SimpleOrmApplication");
        //}
        private void InitializeSessionFactory()
        {
            //AutoPersistenceModel model = CreateMappings();
            _sessionFactory = Fluently.Configure()
          .Database(PostgreSQLConfiguration.PostgreSQL82
          .ConnectionString(c => c
                .Host("localhost")
                .Port(5433)
                .Database("b2blife")
                .Username("postgres")
                .Password("123")))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<bblifeforcebd>())
                //.ExposeConfiguration(BuildSchema)
                //.ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true, true))
                .BuildSessionFactory();
        }
        private void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false, true);
        }
    }
}
