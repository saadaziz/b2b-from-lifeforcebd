﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;

namespace b2blifeforcebd.DAL_lib.DALFunc.Interfaces
{
    public interface IRepo<T> where T : class 
    {

        IList<T> All();

        bool Add(T item);

        bool Add(IList<T> itemList);
        bool Delete(T item);
        bool update(T item);
        T pickbyID(Int64 id);
        IList<T> FindAll(int firstResult, int numberOfResults);
        IList<T> FindAll(int firstResult, int numberOfResults, params Order[] orders);
        IList<T> FindAll(string idPropertyName, string pvalue);
        IList<T> FindByQuery(string queryString);
        long Count(DetachedCriteria criteria);
        void Dispose();
        //saad//
    }
}
