﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Facilities.NHibernateIntegration;
using NHibernate.Criterion;
using b2blifeforcebd.DAL_lib.DALFunc.Interfaces;
using b2blifeforcebd.DAL_lib;
namespace b2blifeforcebd.DAL_lib.DALFunc.Implementations
{
    public class c_partner_DAO:BaseDAORepository<bblifeforcebd.c_partner_factory>,Ic_partner_factory
    {
        
        public IList<bblifeforcebd.c_partner_factory> GetlistByProduct3(string prod3)
        {
            //return null;
            //NHibernate.Criterion crit = NHibernate.Criterion.DetachedCriteria.For<bblifeforcebd.c_partner_factory>().Add(x=>x.product3==prod3);
            return FindAll("product3",prod3);
        }
    }
}
