﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/saad.css" rel="stylesheet">
    <style type="text/css">
        body
        {
            padding-top: 60px;
            padding-bottom: 40px;
        }
        .sidebar-nav
        {
            padding: 9px 0;
        }
    </style>
    <script language="javascript" src="js/jquery.js"></script>
    <script language="javascript" src="js/bootstrap-transition.js"></script>
    <script language="javascript" src="js/bootstrap-alert.js"></script>
    <script language="javascript" src="js/bootstrap-modal.js"></script>
    <script language="javascript" src="js/bootstrap-dropdown.js"></script>
    <script language="javascript" src="js/bootstrap-scrollspy.js"></script>
    <script language="javascript" src="js/bootstrap-tab.js"></script>
    <script language="javascript" src="js/bootstrap-tooltip.js"></script>
    <script language="javascript" src="js/bootstrap-popover.js"></script>
    <script language="javascript" src="js/bootstrap-button.js"></script>
    <script language="javascript" src="js/bootstrap-collapse.js"></script>
    <script language="javascript" src="js/bootstrap-carousel.js"></script>
    <script language="javascript" src="js/bootstrap-typeahead.js"></script>
    <script language="javascript" type="text/javascript" src="js/bootstrap-affix.js"></script>
    <script language="javascript" type="text/javascript">


        !function ($) {
            $(function () {


                $('#latestselling').carousel({
                    interval: 2000
                });
                $('#latestproducts').carousel({
                    interval: 1500
                });
                $('#latestbuying').carousel({
                    interval: 2200
                });
                $('#myCarousel').carousel();
                $('#newsheadline').carousel({ interval: 3500 });
                $('#featuredbuying').carousel();
                $('#featuredproduts').carousel();
                $('#featuredselling').carousel();

            })
        } (window.jQuery)
        
    </script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
</head>
<body>
    <form id="form1" runat="server">
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </a><a class="brand" href="#">Project name</a>
                <div class="nav-collapse collapse ">
                    <ul class="nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                    <ul class="nav pull-right">
                        <li><a href="#">Link</a> </li>
                        <li class="divider-vertical"></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <h2>
                    Logo</h2>
            </div>
            <div class="span7" style="border: 1px solid beige; border-radius: 5px">
                <div class="btn-group">
                    <a class="btn btn-info homeproductssuppbuyersbtn dropdown-toggle" data-toggle="dropdown"
                        href="#">Products <span class="caret "></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Products</a></li>
                        <li><a href="#">Suppliers</a></li>
                        <li><a href="#">Buyers</a></li>
                    </ul>
                    <asp:TextBox ID="txtsearch" CssClass="searchtext" runat="server"></asp:TextBox>
                    <asp:Button runat="server" CssClass="btn-info" ID="Button1" Text="Search" Height="32px"
                        Width="80px" />
                </div>
            </div>
            <div class="span3 ">
                <asp:HyperLink class="btn btn-success" ID="hplpostbuying" runat="server" Font-Size="12px">Post Buying Request</asp:HyperLink>
                <asp:HyperLink class="btn  btn-warning " ID="HyperLink1" runat="server" Font-Size="12px">Post Selling Request</asp:HyperLink>
                <asp:HyperLink class="btn btn-info " ID="HyperLink2" runat="server" Font-Size="12px">Search Agents&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:HyperLink>
                <asp:HyperLink class="btn btn-primary " ID="HyperLink3" runat="server" Font-Size="12px">Display Products&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:HyperLink>
            </div>
        </div>
        <hr />
        <div class="row-fluid">
            <!-- cat & subcat here -->
            <div class="span2 ">
                <div class="well sidebar-nav">
                    <ul class="nav nav-list">
                        <li class="nav-header">Sidebar</li>
                        <li class="active"><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li class="dropdown-submenu"><a tabindex="-1" href="#">More options</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Link112</a></li>
                                <li><a href="#">Link123</a></li>
                                <li><a href="#">Link12312</a></li>
                            </ul>
                        </li>
                        <li class="nav-header">Sidebar</li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li class="nav-header">Sidebar</li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                        <li><a href="#">Link</a></li>
                    </ul>
                </div>
                <!--/.well -->
            </div>
            <!--/span-->
            <div class="span7">
                <div class="hero-unit">
                    <div id="myCarousel" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="img/Lighthouse.jpg" alt="" />
                                <div class="carousel-caption">
                                    <h4>
                                        First Thumbnail label</h4>
                                    <p>
                                        1st thumbnail.</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="img/Lighthouse.jpg" alt="" />
                                <div class="carousel-caption">
                                    <h4>
                                        Second Thumbnail label</h4>
                                    <p>
                                        2nd thumbnail</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="img/Lighthouse.jpg" alt="" />
                                <div class="carousel-caption">
                                    <h4>
                                        Third Thumbnail label</h4>
                                    <p>
                                        3rd thumbnail.</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="img/Lighthouse.jpg" alt="" />
                                <div class="carousel-caption">
                                    <h4>
                                        fourth Thumbnail label</h4>
                                    <p>
                                        4th thumbnail.</p>
                                </div>
                            </div>
                            <div class="item">
                                <img src="img/Lighthouse.jpg" alt="" />
                                <div class="carousel-caption">
                                    <h4>
                                        fifth Thumbnail label</h4>
                                    <p>
                                        5th thumbnail.</p>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                <!--/row-->
                <div class="row-fluid">
                    <div class="span4">
                        <div id="featuredselling" class="carousel slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    featuredselling
                                </div>
                                <div class="item">
                                    featuredselling
                                </div>
                                <div class="item">
                                    featuredselling
                                </div>
                            </div>
                            <a class="left carousel-control" href="#featuredselling" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#featuredselling"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div id="featuredproduts" class="carousel slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    featuredproduts
                                </div>
                                <div class="item">
                                    featuredproduts
                                </div>
                                <div class="item">
                                    featuredproduts
                                </div>
                            </div>
                            <a class="left carousel-control" href="#featuredproduts" style="visibility: hidden"
                                style="visibility: hidden" data-slide="prev">&lsaquo;</a> <a class="right carousel-control"
                                    href="#featuredproduts" style="visibility: hidden" style="visibility: hidden"
                                    data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div id="featuredbuying" class="carousel slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    featuredbuying
                                </div>
                                <div class="item">
                                    featuredbuying
                                </div>
                                <div class="item">
                                    featuredbuying
                                </div>
                            </div>
                            <a class="left carousel-control" href="#featuredbuying" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#featuredbuying"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div id="latestproducts" class="carousel vertical slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    latestproducts
                                </div>
                                <div class="item">
                                    latestproducts
                                </div>
                                <div class="item">
                                    latestproducts
                                </div>
                            </div>
                            <a class="left carousel-control" href="#latestproducts" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#latestproducts"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div id="latestbuying" class="carousel vertical slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    latestbuying
                                </div>
                                <div class="item">
                                    latestbuying
                                </div>
                                <div class="item">
                                    latestbuying
                                </div>
                            </div>
                            <a class="left carousel-control" href="#latestbuying" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#latestbuying"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                    <div class="span4">
                        <div id="latestselling" class="carousel slide vertical">
                            <div class="carousel-inner">
                                <div class="item active">
                                    latestselling1
                                </div>
                                <div class="item">
                                    latestselling2
                                </div>
                                <div class="item">
                                    latestselling3
                                </div>
                            </div>
                            <a class="left carousel-control" href="#latestselling" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#latestselling"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                </div>
                <!--/news-->
                <div class="row-fluid">
                    <div class="span7">
                        <div id="newsheadline" class="carousel slide vertical">
                            <div class="carousel-inner">
                                <div class="item active">
                                    Headline1<br />
                                    news here<br />
                                    1<br />
                                    2<br />
                                    3<br />
                                </div>
                                <div class="item">
                                    Headline2<br />
                                    2<br />
                                    2<br />
                                    3<br />
                                    5<br />
                                </div>
                                <div class="item">
                                    Headline3<br />
                                    1<br />
                                    2<br />
                                    3<br />
                                    5<br />
                                    6<br />
                                </div>
                            </div>
                            <a class="left carousel-control" href="#newsheadline" style="visibility: hidden"
                                data-slide="prev">&lsaquo;</a> <a class="right carousel-control" href="#newsheadline"
                                    style="visibility: hidden" data-slide="next">&rsaquo;</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--rightbar-->
            <div class="span3 backlighter pull-right hero-unit">
                <div class="tabbable">
                    <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">For Buyers</a></li>
                        <li><a href="#tab2" data-toggle="tab">For Sellers</a></li>
                        <li><a href="#tab3" data-toggle="tab">For Agents</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <p>
                                I'm in Section 1.</p>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <p>
                                Howdy, I'm in Section 2.</p>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <p>
                                Howdy, I'm in Section 3.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
        <!--/row-->
        <hr>
        <footer>
            <div class="row-fluid paddingbot">Corporate Partners</div> 
            <div class="row-fluid paddingbot">Be with US on: </div> 
            <div class="row-fluid pull-left paddingbot">
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">About</li>
                        <hr/>
                        <li><a>Company Information</a></li>
                        <li><a>Site Information</a></li>
                        <li><a>Contact Us</a></li>
                        <li><a>Affliate Program</a></li>
                        <li><a>SiteMap</a></li>
                    </ul>
                </span>    
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">Products</li>
                        <hr/>
                        <li><a>How to Buy & Sell</a></li>
                        <li><a>How to Manage Products</a></li>
                        <li><a>Become a Premium Member</a></li>
                        <li><a>Become a Verified Member</a></li>
                        <li><a>Search Products</a></li> 
                        <li><a>Products Archive</a></li>

                    </ul>
                </span>
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">Buyers & Importers</li>
                        <hr/>
                        <li><a>Post Buying Request</a></li>
                        <li><a>Manage Products</a></li>
                        <li><a>Become a Premium Buyer</a></li>
                        <li><a>Become a Verified Buyer</a></li>
                        <li><a>Search Buyer</a></li>
                        <li><a>Buyer Archive</a></li>
                    </ul>
                </span>
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">Sellers/Exporters</li>
                        <hr/> 
                        <li><a>Post Selling Request</a></li>
                         <li><a>Manage Products</a></li>
                        <li><a>Become a Premium Seller</a></li>
                        <li><a>Become a Verified Seller</a></li>
                        <li><a>Search Seller</a></li>
                        <li><a>Seller Archive</a></li>
                    </ul>
                </span>
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">Tools</li>
                        <hr/> 
                        <li><a>Business Newspaper</a></li>
                        <li><a>Download Chat</a></li>
                        <li><a>Download Business Browser</a></li>
                        <li><a>Subscribe new Buy Leads</a></li>
                        <li><a>Subscribe new Sell Leads</a></li>
                        <li><a>Subscribe new Buyers</a></li>
                        <li><a>Subscribe new Sellers</a></li>
                    </ul>
                </span>
                <span class="span2">
                    <ul class="nav nav-tabs">
                        <li class="pull-left">Support</li>
                        <hr/>          
                        <li><a>Frequently Asked Questions</a></li>

                        <li><a>Live Chat</a></li>
                        <li><a>Suggestions</a></li>
                        <li><a>Complaints</a></li>
                    </ul>
                </span>
            </div> 
            <div class="row-fluid pull-left paddingbot">
                <span class="span2"></span>
                <span class="span7">
                    Statistics: Registered Companies:<asp:Label ID="lblregcompanies" runat="server" Text="Label"></asp:Label>,
                    Business Leads:<asp:Label ID="lblbusinessleads" runat="server" Text="Label"></asp:Label>,
                    Displayed Products:<asp:Label ID="lbldispproducts" runat="server" Text="Label"></asp:Label>, 
                    Daily Messages:<asp:Label ID="lbldailymsgs" runat="server" Text="Label"></asp:Label>,<br/>
                    <span class="span4"></span>Currently Online:<asp:Label ID="lblonlineusers" runat="server" Text="Label"></asp:Label> Users at time here,
                </span>  
            </div>
            <div class="row-fluid">Corporate Partners</div>             
        <span class="span7"></span><p>&copy; Lifeforcebd 2012</p>
      </footer>
    </div>
    <!--/.fluid-container-->
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    </form>
</body>
</html>
