﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace b2blifeforcebd.DAL_lib.DALFunc.Interfaces
{
    public interface Ic_partner_factory:IRepo<bblifeforcebd.c_partner_factory>
    {
        IList<bblifeforcebd.c_partner_factory> GetlistByProduct3(string prod3);
    }
}
