﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace b2blifeforcebd.DAL_lib
{
    

        public class c_accepted_delivery_termsmap : ClassMap<bblifeforcebd.c_accepted_delivery_terms>
        {
            public c_accepted_delivery_termsmap()
            {
                Id(x => x.accepted_delivery_terms_id).Column("accepted_delivery_terms_id");
                Map(x => x.FOB).Column("FOB").Nullable();
                Map(x => x.EXW).Column("EXW").Nullable();
                Map(x => x.FCA).Column("FCA").Nullable();
                Map(x => x.DDP).Column("DDP").Nullable();
                Map(x => x.DAF).Column("DAF").Nullable();
                Map(x => x.CFR).Column("CFR").Nullable();
                Map(x => x.FAS).Column("FAS").Nullable();
                Map(x => x.CPT).Column("CPT").Nullable();
                Map(x => x.DDU).Column("DDU").Nullable();
                Map(x => x.DES).Column("DES").Nullable();
                Map(x => x.CIF).Column("CIF").Nullable();
                Map(x => x.CIP).Column("CIP").Nullable();
                Map(x => x.DEQ).Column("DEQ").Nullable();
                Map(x => x.Express_Delivery).Column("Express_Delivery").Nullable();
                Table("c_accepted_delivery_terms");
            }
        }

    public class c_accepted_payment_currencymap : ClassMap<bblifeforcebd.c_accepted_payment_currency>
    {
        public c_accepted_payment_currencymap()
        {
            Id(x => x.accepted_payment_currency_id).Column("accepted_payment_currency_id");
            Map(x => x.USD).Column("USD").Nullable();
            Map(x => x.CAD).Column("CAD").Nullable();
            Map(x => x.GBP).Column("GBP").Nullable();
            Map(x => x.EUR).Column("EUR").Nullable();
            Map(x => x.AUD).Column("AUD").Nullable();
            Map(x => x.CNY).Column("CNY").Nullable();
            Map(x => x.JPY).Column("JPY").Nullable();
            Map(x => x.HKD).Column("HKD").Nullable();
            Map(x => x.CHF).Column("CHF").Nullable();
            Table("c_accepted_payment_currency");
        }

    }
    

    public class c_accepted_payment_typemap : ClassMap<bblifeforcebd.c_accepted_payment_type>
    {
        public c_accepted_payment_typemap()
        {
            Id(x => x.accepted_payment_type_id).Column("accepted_payment_type_id");
            Map(x => x.T_T).Column("T_T").Nullable();
            Map(x => x.moneyGram).Column("MoneyGram").Nullable();
            Map(x => x.western_Union).Column("Western_Union").Nullable();
            Map(x => x.L_C).Column("L_C").Nullable();
            Map(x => x.credit_card).Column("Credit_Card").Nullable();
            Map(x => x.cash).Column("Cash").Nullable();
            Map(x => x.DPDA).Column("D_P_D_A").Nullable();
            Map(x => x.paypal).Column("Paypal").Nullable();
            Map(x => x.escrow).Column("Escrow").Nullable();
            Table("c_accepted_payment_type");
        }
    }
    public class c_brandmap : ClassMap<bblifeforcebd.c_brand>
    {
        public c_brandmap()
        {
            Id(x => x.brand_id).Column("brand_id");
            Map(x => x.brand_name).Column("brand_name").Not.Nullable();
            Map(x => x.reg_certification_photo).Column("reg_certification_photo").Nullable();
            Map(x => x.c_intro_photo_id).Column("c_intro_photo_id").Not.Nullable();
            Table("c_brand");

        }
    }
    public class c_company_detailsmap : ClassMap<bblifeforcebd.c_company_details>
    {
        public c_company_detailsmap()
        {
            Id(x => x.company_id).Column("company_id");
            Map(x => x.user_id).Column("T/user_id").Not.Nullable();
            Map(x => x.company_name).Column("company_name").Not.Nullable();
            Map(x => x.subdomain_name).Column("subdomain_name").Not.Nullable();
            Map(x => x.address_id).Column("address_id").Not.Nullable();
            Map(x => x.main_products).Column("main_products").Not.Nullable();
            Map(x => x.other_products).Column("other_products").Nullable();
            Map(x => x.company_reg_year).Column("company_reg_year").Not.Nullable();
            Map(x => x.company_website).Column("company_website").Nullable();
            Map(x => x.legal_owner).Column("legal_owner").Not.Nullable();
            Map(x => x.office_size).Column("office_size").Not.Nullable();
            Map(x => x.company_certification).Column("company_certification").Not.Nullable();
            Map(x => x.factory_info_id).Column("factory_info_id").Not.Nullable();
            Map(x => x.business_type_id).Column("business_type_id").Not.Nullable();
            Map(x => x.total_no_of_employees).Column("total_no_of_employees").Not.Nullable();
            Table("c_company_details");
        }
    }
    public class c_introduction_and_imagesmap : ClassMap<bblifeforcebd.c_introduction_and_images>
    {
        public c_introduction_and_imagesmap()
        {
            Id(x => x.c_intro_image_id).Column("c_intro_image_id");
            Map(x => x.company_logo).Column("company_logo").Not.Nullable();
            Map(x => x.detailed_company_intro).Column("detailed_company_intro").Not.Nullable();
            Map(x => x.company_brand_id).Column("company_brand_id").Not.Nullable();
            Map(x => x.trade_show_attended_id).Column("trade_show_attended_id").Nullable();
            Table("c_introduction_and_images");

        }
    }
    public class c_language_spokenmap : ClassMap<bblifeforcebd.c_language_spoken>
    {
        public c_language_spokenmap()
        {
            Id(x => x.language_id).Column("language_id");
            Map(x => x.English).Column("English").Nullable();
            Map(x => x.Japanese).Column("Japanese").Nullable();
            Map(x => x.Arabic).Column("Arabic").Nullable();
            Map(x => x.Korean).Column("Korean").Nullable();
            Map(x => x.Chinese).Column("Chinese").Nullable();
            Map(x => x.Portuguse).Column("Portuguse").Nullable();
            Map(x => x.French).Column("French").Nullable();
            Map(x => x.Hindi).Column("Hindi").Nullable();
            Map(x => x.Spanish).Column("Spanish").Nullable();
            Map(x => x.German).Column("German").Nullable();
            Map(x => x.Russian).Column("Russian").Nullable();
            Map(x => x.Italian).Column("Italian").Nullable();
            Table("c_language_spoken");
        }
    }
    public class c_partner_factorymap : ClassMap<bblifeforcebd.c_partner_factory>
    {
        public c_partner_factorymap()
        {
            Id(x => x.partner_factory_id).Column("partner_factory_id");
            Map(x => x.factory_name).Column("factory_name").Nullable();
            Map(x => x.cooperation_contract).Column("cooperation_contract").Nullable();
            Map(x => x.years_of_cooperation).Column("years_of_cooperation").Nullable();
            Map(x => x.transaction_amnt_prev_year).Column("transaction_amnt_prev_year").Nullable();
            Map(x => x.product1).Column("product1").Nullable();
            Map(x => x.product2).Column("product2").Nullable();
            Map(x => x.product3).Column("product3").Nullable();
            Map(x => x.annual_production_volume1).Column("annual_production_volume1").Nullable();
            Map(x => x.annual_production_volume2).Column("annual_production_volume2").Nullable();
            Map(x => x.annual_production_volume3).Column("annual_production_volume3").Nullable();
            Table("c_partner_factory");
        }

    }
    public class c_trade_informationmap : ClassMap<bblifeforcebd.c_trade_information>
    {
        public c_trade_informationmap()
        {
            Id(x => x.trade_info_id).Column("trade_info_id");
            Map(x => x.total_annual_sales_volume).Column("total_annual_sales_volume").Not.Nullable();
            Map(x => x.export_percentage).Column("export_percentage").Not.Nullable();
            Map(x => x.main_markets).Column("main_markets").Not.Nullable();
            Map(x => x.main_customers).Column("main_customers").Not.Nullable();
            Map(x => x.no_of_employees_in_trade_dept).Column("no_of_employees_in_trade_dept").Not.Nullable();
            Map(x => x.nearest_port).Column("nearest_port").Not.Nullable();
            Map(x => x.avg_lead_time).Column("avg_lead_time").Not.Nullable();
            Map(x => x.province).Column("province").Nullable();
            Map(x => x.country_id).Column("country_id").Nullable();
            Map(x => x.minimum_order_value).Column("minimum_order_value").Not.Nullable();
            Map(x => x.accepted_delivery_term_id).Column("accepted_delivery_term_id").Not.Nullable();
            Map(x => x.accepted_payment_currency_id).Column("accepted_payment_currency_id").Not.Nullable();
            Map(x => x.accepted_payment_type_id).Column("accepted_payment_type_id").Not.Nullable();
            Map(x => x.language_spoken_id).Column("language_spoken_id").Not.Nullable();
            Table("c_trade_information");
        }
    }
    public class c_trade_showmap : ClassMap<bblifeforcebd.c_trade_show>
    {
        public c_trade_showmap()
        {
            Id(x => x.trade_show_id).Column("trade_show_id");
            Map(x => x.trade_show_name).Column("trade_show_name").Not.Nullable();
            Map(x => x.date_attended).Column("date_attended").Not.Nullable();
            Map(x => x.host_country).Column("host_country").Not.Nullable();
            Table("c_trade_show");
        }

    }
    public class p_productmap : ClassMap<bblifeforcebd.p_product>
    {
        public p_productmap()
        {
            Id(x => x.product_id).Column("product_id");
            Map(x => x.product_title).Column("product_title").Not.Nullable();
            Map(x => x.cat_id).Column("cat_id").Not.Nullable();
            Map(x => x.country_id).Column("country_id").Not.Nullable();
            Map(x => x.min_quantity).Column("min_quantity").Not.Nullable();
            Map(x => x.min_price).Column("min_price").Not.Nullable();
            Map(x => x.max_price).Column("max_price").Not.Nullable();
            Map(x => x.created_date).Column("created_date").Not.Nullable();
            Map(x => x.availability).Column("availability").Not.Nullable();
            Map(x => x.specification_id).Column("specification_id").Not.Nullable();
            Map(x => x.user_id).Column("user_id").Not.Nullable();
            Map(x => x.photo_id).Column("photo_id").Not.Nullable();
            Table("p_product");

        }

    }
    public class p_product_specificationmap : ClassMap<bblifeforcebd.p_product_specification>
    {
        public p_product_specificationmap()
        {
            Id(x => x.spec_id).Column("spec_id");
            Map(x => x.spec_name).Column("spec_name").Not.Nullable();
            Map(x => x.user_id).Column("user_id").Not.Nullable();
            Map(x => x.rich_description).Column("rich_description").Not.Nullable();
            Map(x => x.meta_keyword).Column("meta_keyword").Nullable();
            Map(x => x.meta_description).Column("meta_description").Nullable();
            Table("p_product_specification");
        }

    }
    public class tb_business_typemap : ClassMap<bblifeforcebd.tb_business_type>
    {
        public tb_business_typemap()
        {
            Id(x => x.business_type_id).Column("business_type_id");
            Map(x => x.manufacturer).Column("manufacturer").Nullable();
            Map(x => x.trading_company).Column("trading_company").Nullable();
            Map(x => x.buying_office).Column("buying_office").Nullable();
            Map(x => x.agent).Column("agent").Nullable();
            Map(x => x.govt_ministry_bureau_commission).Column("govt_ministry_bureau_commission").Nullable();
            Map(x => x.distributor_wholesaler).Column("distributor_wholesaler").Nullable();
            Map(x => x.association).Column("association").Nullable();
            Map(x => x.business_service).Column("business_service").Nullable();
            Map(x => x.other).Column("other").Nullable();
            Table("tb_business_type");
        }
    }
    public class tb_contactmap : ClassMap<bblifeforcebd.tb_contact>
    {
        public tb_contactmap()
        {
            Id(x => x.contact_id).Column("contact_id");
            Map(x => x.email).Column("email").Nullable();
            Map(x => x.street).Column("street").Not.Nullable();
            Map(x => x.city).Column("city").Not.Nullable();
            Map(x => x.state).Column("state").Not.Nullable();
            Map(x => x.zipcode).Column("zipcode").Not.Nullable();
            Map(x => x.country_id).Column("country_id").Not.Nullable();
            Map(x => x.telephone).Column("telephone").Not.Nullable();
            Map(x => x.Fax).Column("Fax").Nullable();
            Table("tb_contact");
        }

    }

    public class tb_countrymap : ClassMap<bblifeforcebd.tb_country>
    {
        public tb_countrymap()
        {
            Id(x => x.country_id).Column("country_id");
            Map(x => x.country_name).Column("country_name").Not.Nullable();
            Map(x => x.country_code).Column("country_code").Not.Nullable();
            Map(x => x.ISDcode).Column("ISDcode").Nullable();
            Table("tb_country");
        }
    }
    public class tb_photo_bankmap : ClassMap<bblifeforcebd.tb_photo_bank>
    {
        public tb_photo_bankmap()
        {
            Id(x => x.photo_id).Column("photo_id");
            Map(x => x.up_name).Column("up_name").Not.Nullable();
            Map(x => x.server_name).Column("server_name").Not.Nullable();
            Map(x => x.group_id).Column("group_id").Nullable();
            Map(x => x.extension).Column("extension").Not.Nullable();
            Map(x => x.height).Column("height").Not.Nullable();
            Map(x => x.width).Column("width").Not.Nullable();
            Table("tb_photo_bank");
        }

    }
    public class u_more_informationmap : ClassMap<bblifeforcebd.u_more_information>
    {
        public u_more_informationmap()
        {
            Id(x => x.more_info_id).Column("more_info_id");
            Map(x => x.industry_we_are_in).Column("industry_we_are_in").Nullable();
            Map(x => x.pname1).Column("pname1").Nullable();
            Map(x => x.pname2).Column("pname2").Nullable();
            Map(x => x.pname3).Column("pname3").Nullable();
            Map(x => x.cat_id1).Column("cat_id1").Nullable();
            Map(x => x.cat_id2).Column("cat_id2").Nullable();
            Map(x => x.cat_id3).Column("cat_id3").Nullable();
            Map(x => x.purchasing_frequency).Column("purchasing_frequency").Not.Nullable();
            Map(x => x.annual_purchase_volume).Column("annual_purchase_volume").Not.Nullable();
            Map(x => x.country_id1).Column("country_id1").Nullable();
            Map(x => x.country_id2).Column("country_id2").Nullable();
            Map(x => x.country_id3).Column("country_id3").Nullable();
            Map(x => x.business_type_id).Column("business_type_id").Not.Nullable();
            Map(x => x.company_address).Column("company_address").Not.Nullable();
            Map(x => x.company_introduction).Column("company_introduction").Not.Nullable();
            Map(x => x.company_logo).Column("company_logo ").Not.Nullable();
            Map(x => x.company_license1).Column("company_license1").Not.Nullable();
            Map(x => x.company_license2).Column("company_license2").Not.Nullable();
            Map(x => x.company_license3).Column("company_license3").Not.Nullable();
            Table("u_more_information");
        }
    }
    public class u_user_profilemap : ClassMap<bblifeforcebd.u_user_profile>
    {
        public u_user_profilemap()
        {
            Id(x => x.user_id).Column("user_id");
            Map(x => x.first_name).Column("first_name").Not.Nullable();
            Map(x => x.last_name).Column("last_name").Nullable();
            Map(x => x.gender).Column("gender").Not.Nullable();
            Map(x => x.job_title).Column("job_title").Nullable();
            Map(x => x.email).Column("email").Nullable();
            Map(x => x.address).Column("address").Not.Nullable();
            Map(x => x.company_name).Column("company_name").Nullable();
            Map(x => x.preferred_business_type).Column("preferred_business_type").Not.Nullable();
            Map(x => x.website).Column("website").Nullable();
            Map(x => x.password).Column("password").Not.Nullable();
            Table("u_user_profile");
        }

    }

    public class p_categorymap : ClassMap<bblifeforcebd.p_category>
    {
        public p_categorymap()
        {
            Id(x => x.cat_id).Column("cat_id");
            Map(x => x.cat_name).Column("cat_name").Not.Nullable();
            Map(x => x.parent_cat_id).Column("parent_cat_id").Nullable();
            Map(x => x.category_chain).Column("category_chain").Nullable();
            Table("p_category");
        }
    }
    public class p_required_fieldmap : ClassMap<bblifeforcebd.p_required_field>
    {
        public p_required_fieldmap()
        {
            Id(x => x.required_field_id).Column("required_field_id");
            Map(x => x.required_field_name).Column("required_field_name").Not.Nullable();
            Map(x => x.field_type).Column("field_type").Not.Nullable();
            Map(x => x.is_required).Column("is_required").Not.Nullable();
            Table("p_required_field");
        }
    }
    public class p_child_fieldmap : ClassMap<bblifeforcebd.p_child_field>
    {
        public p_required_fieldmap()
        {
            Id(x => x.child_field_id).Column("child_field_id");
            Map(x => x.child_field_name).Column("child_field_name").Not.Nullable();
            Map(x => x.nested_field_id).Column("nested_field_id").Not.Nullable();
            Map(x => x.required_field_id).Column("required_field_id").Not.Nullable();
            Table("p_child_field");
        }
    }
    public class p_fields_under_catmap : ClassMap<bblifeforcebd.p_fields_under_cat>
    {
        public p_required_fieldmap()
        {
            Id(x => x.fields_under_cat_id).Column("fields_under_cat_id");
            Map(x => x.cat_id).Column("cat_id").Not.Nullable();
            Map(x => x.field_name).Column("field_name").Not.Nullable();
            Map(x => x.required_field_id).Column("required_field_id").Not.Nullable();
            Table("p_fields_under_cat");
        }
    }



}
